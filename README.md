# Corekeepers Helmcharts

## Summary

This repository allows you to deploy easily a CoreKeepers dedicated server on Kubernetes.
It's pretty useful if you have a server around, or another machine that you are not using.

## Prerequisites

There are a few prerequisites to deploy this charts:

- A Kubernetes cluster
- `kubectl` CLI tool
- `helm` CLI tool

Your mileage may vary but for a more in-depth installation, here was my setup and its installation steps:

**1. Installation of a k3s cluster (lightweight Kubernetes that can even run on a RaspberryPi :D )**

You can have a look at the documentation on: https://docs.k3s.io/quick-start

_By default, k3s is installed with Traefik. I wanted to use a classic Nginx ingress._

**If you don't understand what I'm talking about, it's okay ! You just have to run the following:**

`curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable traefik" sh -`

After that, you Kubernetes cluster should be up and running !
Note: You must have access to root privileges to install k3s

**2. (Optional) Installation of `kubectl` CLI**

**`kubectl` should have been installed already and automatically with the last step**
To be sure you can run: `kubectl version`

If it works correctly, you will have something like this:
```shell
>Client Version: version.Info{<SOME STUFF>}
>Kustomize Version: v<SOME VERSION>
>Server Version: version.Info{<SOME MORE STUFF}
```

**Since you have `kubectl` installed, you can skip to the next step**

Kubernetes has a well documented page on how to install the tool: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/

But if you don't plan on using it more than for CoreKeepers, you can follow this:

- Download the latest release:
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"`

- Make available the command system-wide:
```shell
chmod +x kubectl
mv ./kubectl /usr/local/bin/
```

**3. Installation of `helm` CLI**

Again, the documentation is well written if you want to have a look: https://helm.sh/docs/intro/install/

But if you are not sure, you can use the "Installation from Script"

```shell
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

When done, you can check the installation with: `helm version`
It will output something like:

```shell
>version.BuildInfo{<SomeStuff>}
```

After this, you should be operational to deploy some stuff on your K3S cluster !

**Note: Keep exploring ! There is so much you can deploy on Kubernetes, especially with HelmCharts ;)**

## Deploy the chart

HelmCharts can be packaged, thing I did in this repository.
You can find all the releases I published here : https://gitlab.com/adrian.salas/corekeepers-helmcharts/-/releases

To retrieve the chart (be sure to be using the latest version):

```shell
wget -c https://gitlab.com/adrian.salas/corekeepers-helmcharts/uploads/21ebe295136f251bd8ecb5f94f51b093/CoreKeepers-0.1.0.tgz -O - | tar -xz
```

Then you just have to run:

```shell
helm upgrade --install -n corekeepers corekeeper-server CoreKeepers/. --create-namespace
```

And done ! You server is up and running (you might have to wait some minutes).

Notes 
- To retrieve the game ID :

```shell
kubectl -n corekeepers logs $(kubectl -n corekeepers get pods --selector=katenary.io/component=corekeeper,katenary.io/resource=deployment -o jsonpath='{.items[0].metadata.name}') | grep --text "Game ID"
```
If you don't have ID, wait a few minutes.
If still nothing, something might have gone wrong :( 

- If you have an error on the `helm upgrade`:

```shell
Error: INSTALLATION FAILED: Kubernetes cluster unreachable: Get "http://localhost:8080/version"
```

You can run `export KUBECONFIG=/etc/rancher/k3s/k3s.yaml` and then `helm upgrade [...]` again.


## Support
You can open issues if you find any problems with the charts, or if you might feel you missed something :)


## Acknowledgment
Based on the work of: https://github.com/escapingnetwork/core-keeper-dedicated for the Docker image
